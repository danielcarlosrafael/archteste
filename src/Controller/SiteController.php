<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class SiteController extends AbstractController
{

    /**
     * @Route("/", name="site_index", methods={"GET"})
     */
    public function index(): Response
    {
		$search = trim($_REQUEST['search']) ?? null;

    	$query = "SELECT company.titulo, company.telefone, company.endereco, company.cidade FROM company
    				LEFT JOIN category_company ON category_company.id_company = company.id
					LEFT JOIN category ON category.id = category_company.id_category";

		if (!empty($search)) {
			$query .= " WHERE (
						company.titulo LIKE :search OR
						company.endereco LIKE :search OR
						company.cep LIKE :search OR
					    company.cidade LIKE :search OR
					    category.titulo LIKE :search
					   )";
		}

		$query .= " GROUP BY company.id";

        $em = $this->getDoctrine()->getManager();
        $statement = $em->getConnection()->prepare($query);
        if (!empty($search)) $statement->bindValue('search', "%{$search}%");
        $statement->execute();

        $result = $statement->fetchAll();

        return $this->render('site/index.html.twig', [
            'companies' => $result,
            'search' => $search
        ]);
    }
}