<?php

namespace App\Entity;

use App\Repository\CompanyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CompanyRepository::class)
 */
class CategoryCompany
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $id_category;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $id_company;


    public function getId(): ?int
    {
        return $this->id;
    }


    public function getIdCategory(): ?int
    {
        return $this->id_category;
    }

    public function setIdCategory(?int $id_category): self
    {
        $this->id_category = $id_category;

        return $this;
    }


    public function getIdCompany(): ?int
    {
        return $this->id_company;
    }

    public function setIdCompany(?int $id_company): self
    {
        $this->id_company = $id_company;

        return $this;
    }

}